package secondTask;

import secondTask.Command.*;

public class FirstCommand implements ICommand{
    private String name = "First";

    private ICommand cmd = (str) -> {
        System.out.println(name + " command printed " + str);
    };


    @Override public void execute(String str) {
        cmd.execute(str);
    }
}
