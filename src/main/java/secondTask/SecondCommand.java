package secondTask;

import secondTask.Command.*;

public class SecondCommand implements ICommand {
    private String name = "Second";

    private ICommand cmd = new ICommand() {
        @Override public void execute(String str) {
            System.out.println(name + " printed " + str);
        }
    };

    @Override public void execute(String str) {
        cmd.execute(str);
    }
}
