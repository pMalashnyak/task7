package firstTask;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.OptionalDouble;

import firstTask.TriFunction.*;

public class Main1 {
    public static void main(String[] args) {


        triFunction frst = (a, b, c) -> {
            List<Integer> list = new ArrayList<Integer>();
            list.add(a);
            list.add(b);
            list.add(c);
            return list
                .stream()
                .max(Comparator.<Integer>naturalOrder())
                .get();
        };

        triFunction avrg = (a, b, c) -> {
            List<Integer> list = new ArrayList<Integer>();
            list.add(a);
            list.add(b);
            list.add(c);
            OptionalDouble average = list
                .stream()
                .mapToDouble(x -> x)
                .average();
            return (int) average.getAsDouble();
        };

        System.out.println(frst.function(1, 4, 6) + "\n");
        System.out.println(avrg.function(4, 6, 11) + " ");
    }


}
