package thirdTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.*;

public class Generator {
    static public ArrayList<Integer> generate1() {
        return (ArrayList<Integer>) new Random()
            .ints(0, 350)
            .limit(10)
            .boxed()
            .collect(Collectors.toList());
    }
}
