package forthTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Generator {
    private BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    public String line;

    public Generator() throws IOException {
        line = br.readLine();
    }

    public void unique() {
        System.out.println("Unique count: " + Arrays.stream(line.split(" ")).distinct().count());
        System.out.println("Unique sorted: ");
        Arrays.stream(line.split(" ")).distinct().sorted().forEach(a -> System.out.print(a + " "));
    }

    public void countWords() {
        Map<String, Long> map = Arrays.stream(line.split(" "))
            .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        System.out.println("\nWord counts: ");
        System.out.println(map);
    }

    public void countChars() {
        String newLine = line.replaceAll("[^a-z0-9]", "");
        Map<String, Long> map = Arrays.stream(newLine.split(""))
            .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        System.out.println("\nSymbols counts: ");
        System.out.println(map);
    }
}
